﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Threading.Tasks;

namespace WCF_NewsService
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Count() < 1)
            { 
                ServiceHost host = new ServiceHost(typeof(News_Service));
                host.Open(); //must run VS as admin to get port registration
            }
            Console.WriteLine("Host Open Sucessfully ...");
            Console.ReadLine();
        }
    }
}
