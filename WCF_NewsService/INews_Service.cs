﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WCF_NewsService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "INews_Service" in both code and config file together.
    [ServiceContract]
    public interface INews_Service
    {
        [OperationContract]
        void DoWork();
        [OperationContract]
        TOInews GetNews(int a);
    }
    [DataContract]
    public class TOInews
    {
        private int _id;
        private string _header;
        private string _body;
        [DataMember]
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }
        [DataMember]
        public string Header
        {
            get { return _header; }
            set { _header = value; }
        }
        [DataMember]
        public string Body
        {
            get { return _body; }
            set { _body = value; }
        }
    }
    
}
