﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WCF_NewsService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "News_Service" in both code and config file together.
    public class News_Service : INews_Service
    {
        public void DoWork()
        {
        }

        public TOInews GetNews(int a)
        {
            TOInews objtoinews = new TOInews();
            switch (a)
            {
                case 1:
                    {
                        objtoinews.ID = 1;
                        objtoinews.Header = "Mumbai News";
                        objtoinews.Body = "2013 Code contest quiz orgnize by TOI";
                        break;
                    }
                case 2:
                    {
                        objtoinews.ID = 2;
                        objtoinews.Header = "Pune News";
                        objtoinews.Body = "commonwealth fraud in pune ";
                        break;
                    }
                case 3:
                    {
                        objtoinews.ID = 3;
                        objtoinews.Header = "Solapur News";
                        objtoinews.Body = "New IT hub in Solapur";
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
            return objtoinews;
        }
    }
}
